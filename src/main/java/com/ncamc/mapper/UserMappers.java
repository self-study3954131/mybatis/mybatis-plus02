package com.ncamc.mapper;
import java.util.List;
import java.util.Collection;
import org.apache.ibatis.annotations.Param;

import com.ncamc.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.ncamc.entity.User
 */
public interface UserMappers extends BaseMapper<User> {

    //直接添加所有数据
    int insertAll(User user);

    //添加数据判读字段是否为null
    int insertSelective(User user);

    //添加多条数据
    int insertBatch(@Param("userCollection") Collection<User> userCollection);

    //根据ID和年龄删除
    int deleteByIdAndAge(@Param("id") Long id, @Param("age") Integer age);

    //根据ID修改用户名和年龄
    int updateUserNameAndAgeById(@Param("userName") String userName, @Param("age") Integer age, @Param("id") Long id);

    //根据用户名和年龄查询？到？之间的用户
    List<User> selectAllByUserNameAndAgeBetween(@Param("userName") String userName, @Param("beginAge") Integer beginAge, @Param("endAge") Integer endAge);

    //查询用户名、年龄、邮箱、根据年龄降序排序
    List<User> selectAgeAndUserNameAndEmailOrderByAgeDesc();
}