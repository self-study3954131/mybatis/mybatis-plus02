package com.ncamc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ncamc.entity.User;
import com.ncamc.mapper.UserMappers;
import com.ncamc.service.UserService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMappers, User>
    implements UserService{

}




